(* This file is generated automatically by ./configure. *)

module Gettext = Gettext.Program (
  struct
    let textdomain = "guestfs-tools"
    let codeset = None
    let dir = None
    let dependencies = []
  end
) (GettextStub.Native)
