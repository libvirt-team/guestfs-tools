
=head1 名前

virt-drivers - Detect bootloader, kernel and drivers inside guest

=head1 書式

 virt-drivers [--options] -d domname

 virt-drivers [--options] -a disk.img

=head1 説明

This tool can detect the bootloader, kernel and drivers inside some guests from only the disk image.  It can detect, for example, whether a disk image needs BIOS or UEFI to boot, and whether it supports virtio or requires slower emulated devices.

=head2 Notes

Normally you should distribute hypervisor metadata (eg. libvirt XML or OVF) alongside disk images to tell the hypervisor how to boot them.  This tool is used when this metadata has not been provided.  Work with the supplier of the disk image to get them to provide proper metadata.

=head1 XML フォーマット

The output is an XML document.  At the top level it lists the operating systems found (the same as L<virt-inspector(1)>):

 <operatingsystems>
   <operatingsystem>
     <root>/dev/sda2</root>
     <name>linux</name>
     <arch>x86_64</arch>
     <distro>fedora</distro>
     ...
   </operatingsystem>
   <operatingsystem>
     <root>/dev/sdb1</root>
     ...
   </operatingsystem>
 </operatingsystems>

=head2 E<lt>firmwareE<gt>

The E<lt>firmwareE<gt> element lists the firmware which is required to boot the guest.  For UEFI it will additionally show the EFI system partition ("ESP").  Guests may support multiple boot firmwares.  For example this guest is detected as using UEFI boot, and the UEFI ESP is the first partition of the first disk:

 <operatingsystems>
   <firmware type='uefi'>/dev/sda1</firmware>
   <operatingsystem>
     ...

=head2 E<lt>bootloaderE<gt> and E<lt>kernelE<gt>

The E<lt>bootloaderE<gt> element shows the bootloader found in the Linux guest.  If known, this may contain information about what Linux kernels are provided.  For example:

 <operatingsystems>
   <firmware type='bios'/>
   <operatingsystem>
     <root>/dev/sda2</root>
     <name>linux</name>
     ...
     <bootloader type='grub2' config='/boot/grub2/grub.cfg'>
       <kernel>
         <name>kernel</name>
         <version>6.1.0-0.rc6.46.fc38.x86_64</version>
         <vmlinuz>/boot/vmlinuz-6.1.0-0.rc6.46.fc38.x86_64</vmlinuz>
         <modules>
           ...
         </modules>
         <supports_virtio_blk/>
         <supports_virtio_net/>
         ...
       </kernel>
     </bootloader>

Many more fields are usually available for Linux guests, including a complete list of kernel modules and information about support for virtio. For a complete example see: L<https://github.com/libguestfs/guestfs-tools/tree/master/drivers>

=head2 E<lt>driversE<gt>

The E<lt>driversE<gt> element lists information about drivers found in Windows guests:

 <operatingsystems>
   <firmware type='bios'/>
   <operatingsystem>
     <root>/dev/sda2</root>
     <name>windows</name>
     ...
     <drivers>
       <driver>
         <name>scsidev</name>
         <pci vendor='1077' device='1216' subsystem='8471101E'/>
         <pci vendor='1077' device='1216' subsystem='8493101E'/>
       </driver>
       ...
     </drivers>

The driver name (eg. C<scsidev>) corresponds to the Windows driver F<.INF> file (eg. F<scsidev.inf>).  The list of PCI, USB etc devices are the matching devices which would cause this driver to load at boot.

=head1 オプション

=over 4

=item B<--help>

ヘルプを表示します。

=item B<-a> file

=item B<--add> file

仮想マシンからディスクイメージの I<file> を追加します。

ディスクイメージの形式は自動的に検出されます。強制的に特定の形式を使用するには I<--format> オプションを使用します。

=item B<-a> URI

=item B<--add> URI

Add a remote disk.  The URI format is compatible with guestfish.  See L<guestfish(1)/ADDING REMOTE STORAGE>.

=item B<--blocksize> B<512>

=item B<--blocksize> B<4096>

This parameter sets the sector size of the disk image added with I<-a> option and is ignored for libvirt guest added with I<-d> option.  See also L<guestfs(3)/guestfs_add_drive_opts>.

=item B<--colors>

=item B<--colours>

Use ANSI colour sequences to colourize messages.  This is the default when the output is a tty.  If the output of the program is redirected to a file, ANSI colour sequences are disabled unless you use this option.

=item B<-c> URI

=item B<--connect> URI

libvirt を使用していると、指定された I<URI> に接続します。  省略すると、デフォルトの libvirt ハイパーバイザーに接続します。

ゲストのブロックデバイスを直接指定していると（(I<-a>)）、libvirt は何も使用されません。

=item B<-d> guest

=item B<--domain> guest

名前付きの libvirt 仮想マシンからすべてのディスクを追加します。  名前の代わりに仮想マシンの UUID を使用できます。

=item B<--echo-keys>

When prompting for keys and passphrases, virt-get-kernel normally turns echoing off so you cannot see what you are typing.  If you are not worried about Tempest attacks and there is no one else in the room you can specify this flag to see what you are typing.

=item B<--format> raw|qcow2|..

=item B<--format> auto

The default for the I<-a> option is to auto-detect the format of the disk image.  Using this forces the disk format for the I<-a> option on the command line.

仮想マシンのディスクイメージが信頼できない raw 形式である場合、 ディスク形式を指定するためにこのオプションを使用すべきです。 これにより、悪意のある仮想マシンにより起こり得る セキュリティ問題を回避できます (CVE-2010-3851)。

__INCLUDE:key-option.pod__

__INCLUDE:keys-from-stdin-option.pod__

=item B<-q>

=item B<--quiet>

Don’t print ordinary progress messages.

=item B<-v>

=item B<--verbose>

デバッグ用の冗長なメッセージを有効にします。

=item B<-V>

=item B<--version>

バージョン番号を表示して、終了します。

=item B<--wrap>

Wrap error, warning, and informative messages.  This is the default when the output is a tty.  If the output of the program is redirected to a file, wrapping is disabled unless you use this option.

=item B<-x>

libguestfs API 呼び出しのトレースを有効にします。

=back

=head1 環境変数

For other environment variables which affect all libguestfs programs, see L<guestfs(3)/ENVIRONMENT VARIABLES>.

=head1 終了ステータス

このプログラムは、成功すると 0 を、エラーがあると 0 以外を返します。

=head1 関連項目

L<guestfs(3)>, L<guestfish(1)>, L<guestmount(1)>, L<virt-get-kernel(1)>, L<virt-inspector(1)>, L<virt-v2v(1)>, L<http://libguestfs.org/>.

=head1 著者

Richard W.M. Jones L<http://people.redhat.com/~rjones/>

=head1 COPYRIGHT

Copyright (C) 2009-2023 Red Hat Inc.

